
public class PersonalDetails {

	private String name;
	private String address;
	private String cellNo;
	private String emailId;
	private String linkedInId;
	
	
	
	
	public PersonalDetails() {
		super();
		new Singleton();
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCellNo() {
		return cellNo;
	}
	public void setCellNo(String cellNo) {
		this.cellNo = cellNo;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getLinkedInId() {
		return linkedInId;
	}
	public void setLinkedInId(String linkedInId) {
		this.linkedInId = linkedInId;
	}
	@Override
	public String toString() {
		return "PersonalDetails [name=" + name + ", address=" + address + ", cellNo=" + cellNo + ", emailId=" + emailId
				+ ", linkedInId=" + linkedInId + "]";
	}

	
	
	
}
